cmake_minimum_required(VERSION 3.17)
project(npecs)

set(CMAKE_CXX_STANDARD 17)

set(FETCHCONTENT_QUIET OFF)
include(FetchContent)

include(cmake/np.cmake)

if (NOT DEFINED NPECS_IS_LIBRARY)
    include(cmake/import_doctest.cmake)
    include(cmake/import_boost.cmake)
    include(cmake/import_libbacktrace.cmake)

    add_subdirectory(test)
    add_subdirectory(cpptest)
    add_subdirectory(showcase)
endif()

add_subdirectory(src)