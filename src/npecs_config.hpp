
#ifndef NPECS_CONFIG_HPP
#define NPECS_CONFIG_HPP

namespace npecs {
    /**
     * should be implemented by client application
     */
    void error(const std::string& message);
}

#endif //NPECS_CONFIG_HPP
