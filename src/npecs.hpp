#ifndef NPECS_NPECS_H
#define NPECS_NPECS_H

#include "npecs/internal/IComponentPool.hpp"
#include "npecs/internal/InternalComponentRef.hpp"
#include "npecs/internal/IViewIndex.hpp"
#include "npecs/NpEcsException.hpp"

#include "npecs/internal/__/AnyEntity__.hpp"
#include "npecs/internal/__/ComponentPool__.hpp"
#include "npecs/internal/__/EntityKey__.hpp"
#include "npecs/internal/__/ExcludeBy__.hpp"
#include "npecs/internal/__/Storage__.hpp"
#include "npecs/internal/__/ViewIndex__.hpp"
#include "npecs/__/Entity__.hpp"
#include "npecs/__/Query__.hpp"
#include "npecs/__/View__.hpp"
#include "npecs/__/ViewCursor__.hpp"
#include "npecs/__/ViewIterator__.hpp"
#include "npecs/__/World__.hpp"

#endif //NPECS_NPECS_H
