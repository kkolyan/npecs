
#ifndef NPECS_WORLD___HPP
#define NPECS_WORLD___HPP

#include "npecs/World.hpp"
#include "npecs/internal/AnyEntity.hpp"
#include "npecs/Entity.hpp"
#include "npecs/internal/EntityKey.hpp"

namespace npecs {

    template<typename... TComponents>
    View<TComponents...> World::getView() {
        return View<TComponents...>(
                storage.getViewIndex<internal::AnyEntity<Entity>, TComponents...>()
        );
    }

    template<typename TComponent>
    std::vector<typename npecs::ComponentInfo<TComponent>> World::getComponentsOfType() {
        return storage.template getComponentsOfType<TComponent>();
    }

    inline Entity World::newEntity() {
        return {&storage, storage.newEntity()};
    }

    inline std::vector<Entity> World::getAllEntities() {
        std::vector<Entity> results;
        for (const auto& entityKey : storage.getAllActiveEntities()) {
            results.emplace_back(&storage, entityKey);
        }
        return results;
    }

    inline std::vector<typename npecs::ComponentInfo<void>> World::getAllComponents() {
        return storage.getAllComponents();
    }

    template<typename... TComponents>
    Query<TComponents...> World::query() {
        return Query<TComponents...>(storage);
    }

    inline std::size_t World::deleteUnusedViews() {
        return storage.deleteUnusedViews();
    }
}

template<>
inline npecs::Entity npecs::internal::Storage<npecs::Entity>::resolveRichEntityReference(const npecs::internal::EntityKey& entity) {
    return Entity(this, entity);
}

template<>
inline const npecs::internal::EntityKey& npecs::internal::resolveEntityKey<typename npecs::Entity>(const npecs::Entity& entity) {
    return entity.key;
}

#endif //NPECS_WORLD___HPP
