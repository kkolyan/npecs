
#ifndef NPECS_ENTITY___HPP
#define NPECS_ENTITY___HPP

#include "npecs/Entity.hpp"

namespace npecs {

    template<typename TComponent>
    const Entity& Entity::add(TComponent t) const {
        storage->addComponent(key, *this, t);
        return *this;
    }

    template<typename TComponent>
    void Entity::del() const {
        storage->template removeComponentOf<TComponent>(key);
    }

    template<typename TComponent>
    TComponent& Entity::get() const {
        return storage->template getComponentOf<TComponent>(key);
    }

    template<typename TComponent>
    bool Entity::has() const {
        return storage->template hasComponentOf<TComponent>(key);
    }

    inline Entity::Entity() : storage(nullptr) {}

    inline Entity::Entity(internal::Storage<typename npecs::Entity>* storage, const internal::EntityKey& key) :
            storage(storage),
            key(key) {}

    inline void Entity::destroy() const {
        storage->destroyEntity(key);
    }

    inline bool Entity::isAlive() const {
        return storage->isAlive(key);
    }

    inline bool Entity::operator==(const Entity& rhs) const {
        return storage == rhs.storage &&
               key == rhs.key;
    }

    inline bool Entity::operator!=(const Entity& rhs) const {
        return !(rhs == *this);
    }

    inline std::ostream& operator<<(std::ostream& os, const Entity& entity) {
        os << "Entity("
           << "storage: " << entity.storage
           << ", key: " << entity.key
           << ")";
        return os;
    }
}

#endif //NPECS_ENTITY___HPP
