
#ifndef NPECS_VIEWCURSOR___HPP
#define NPECS_VIEWCURSOR___HPP

#include "npecs/ViewCursor.hpp"

namespace npecs {

    template<typename TEntityFilter, typename... TComponents>
    const Entity& ViewCursor<TEntityFilter, TComponents...>::getEntity() {
        return view->getEntity(idx);
    }

    template<typename TEntityFilter, typename... TComponents>
    bool
    ViewCursor<TEntityFilter, TComponents...>::operator==(const ViewCursor<TEntityFilter, TComponents...>& o) const {
        return this->idx == o.idx;
    }

    template<typename TEntityFilter, typename... TComponents>
    bool
    ViewCursor<TEntityFilter, TComponents...>::operator!=(const ViewCursor<TEntityFilter, TComponents...>& o) const {
        return this->idx != o.idx;
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...> ViewCursor<TEntityFilter, TComponents...>::operator*() { return *this; }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...>& ViewCursor<TEntityFilter, TComponents...>::operator++() {
        ++idx;
        return *this;
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...>& ViewCursor<TEntityFilter, TComponents...>::operator--() {
        --idx;
        return *this;
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...>::ViewCursor(
            internal::ViewIndex<typename npecs::Entity, TEntityFilter, TComponents...>* view, types::tuple_id idx) :
            idx(idx),
            view(view) {}

    template<typename TEntityFilter, typename... TComponents> template<int i>
    typename ViewCursor<TEntityFilter, TComponents...>::template
    Type<i>& ViewCursor<TEntityFilter, TComponents...>::get() {
        internal::ViewIndex<Entity, TEntityFilter, TComponents...>* v = this->view;
        return v->template getComponent<i>(idx);
    }
}

#endif //NPECS_VIEWCURSOR___HPP
