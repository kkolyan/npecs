
#ifndef NPECS_VIEWITERATOR___HPP
#define NPECS_VIEWITERATOR___HPP

#include "npecs/ViewIterator.hpp"
#include "npecs/ViewCursor.hpp"

namespace npecs {

    template<typename TEntityFilter, typename... TComponents>
    ViewIterator<TEntityFilter, TComponents...>::~ViewIterator() {
        viewIndex->unlock();
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...> ViewIterator<TEntityFilter, TComponents...>::begin() {
        return ViewCursor<TEntityFilter, TComponents...>(viewIndex, types::tuple_id(0));
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewCursor<TEntityFilter, TComponents...> ViewIterator<TEntityFilter, TComponents...>::end() {
        return ViewCursor<TEntityFilter, TComponents...>(viewIndex, types::tuple_id(length));
    }

    template<typename TEntityFilter, typename... TComponents>
    ViewIterator<TEntityFilter, TComponents...>::ViewIterator(
            internal::ViewIndex<Entity, TEntityFilter, TComponents...>* viewIndex,
            std::size_t length
    ) : viewIndex(viewIndex),
        length(length) {

        viewIndex->lock();
    }
}

#endif //NPECS_VIEWITERATOR___HPP
