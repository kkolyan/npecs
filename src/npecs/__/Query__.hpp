
#ifndef NPECS_QUERY___HPP
#define NPECS_QUERY___HPP

#include "npecs/Query.hpp"
#include "npecs/internal/Storage.hpp"
#include "npecs/internal/AnyEntity.hpp"

namespace npecs {

    template<typename... TComponents>
    Query<TComponents...>::Query(typename internal::Storage<Entity>& storage) : storage(storage) {}

    template<typename... TComponents>
    View<TComponents...> Query<TComponents...>::getView() {
        return View<TComponents...>(
                storage.getViewIndex<internal::AnyEntity<Entity>, TComponents...>()
        );
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
#if _MSC_VER && !__clang__
    typename Query<TComponents...>::ExcludeBy<TExcludeByComponents...>
#else
    typename Query<TComponents...>::template ExcludeBy<TExcludeByComponents...>
#endif
    Query<TComponents...>::excludeBy() {
        return Query::ExcludeBy<TExcludeByComponents...>(storage);
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    Query<TComponents...>::ExcludeBy<TExcludeByComponents...>::ExcludeBy(internal::Storage <Entity>& storage)
            : storage(storage) {}

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    typename View<TComponents...>::template ExcludeBy<TExcludeByComponents...>
    Query<TComponents...>::ExcludeBy<TExcludeByComponents...>::getView() {
        return typename View<TComponents...>::template ExcludeBy<TExcludeByComponents...>(
                storage.getViewIndex<internal::ExcludeBy<typename npecs::Entity, TExcludeByComponents...>, TComponents...>()
        );
    }
}

#endif //NPECS_QUERY___HPP
