
#ifndef NPECS_VIEW___HPP
#define NPECS_VIEW___HPP

#include "npecs/View.hpp"
#include "npecs/ViewIterator.hpp"
#include "npecs/World.hpp"

namespace npecs {

    template<typename... TComponents>
    View<TComponents...>::View(
            internal::ViewIndex<
                    Entity,
                    typename npecs::internal::AnyEntity<Entity>,
                    TComponents...
            >* viewIndex
    ) : viewIndex(viewIndex) {
        viewIndex->acquire();
    }

    template<typename... TComponents>
    View<TComponents...>::View(World& world)
            : viewIndex(world.template getView<TComponents...>().viewIndex) {
        viewIndex->acquire();
    }

    template<typename... TComponents>
    View<TComponents...>::~View() {
        viewIndex->release();
    }

    template<typename... TComponents>
    ViewIterator<internal::AnyEntity<Entity>, TComponents...>
    View<TComponents...>::iterate() {
        return ViewIterator<internal::AnyEntity<Entity>, TComponents...>(viewIndex, viewIndex->getEntityCount());
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    View<TComponents...>::ExcludeBy<TExcludeByComponents...>::ExcludeBy(
            internal::ViewIndex<
                    Entity,
                    typename internal::ExcludeBy<Entity, TExcludeByComponents...>,
                    TComponents...
            >* viewIndex
    ) : viewIndex(viewIndex) {
        viewIndex->acquire();
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    View<TComponents...>::ExcludeBy<TExcludeByComponents...>::ExcludeBy(World& world)
            : viewIndex(world
                                .template query<TComponents...>()
                                .template excludeBy<TExcludeByComponents...>()
                                .getView()
                                .viewIndex) {
        viewIndex->acquire();
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    ViewIterator<typename internal::ExcludeBy<Entity, TExcludeByComponents...>, TComponents...>
    View<TComponents...>::ExcludeBy<TExcludeByComponents...>::iterate() {
        return ViewIterator<
                internal::ExcludeBy<Entity, TExcludeByComponents...>,
                TComponents...
        >(viewIndex, viewIndex->getEntityCount());
    }

    template<typename... TComponents>
    template<typename... TExcludeByComponents>
    View<TComponents...>::ExcludeBy<TExcludeByComponents...>::~ExcludeBy() {
        viewIndex->release();
    }
}

#endif //NPECS_VIEW___HPP
