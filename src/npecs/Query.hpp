
#ifndef NPECS_QUERY_HPP
#define NPECS_QUERY_HPP

#include "npecs/View.hpp"

namespace npecs {
    class Entity;

    namespace internal {
        template<typename TEntity>
        class Storage;
    }

    template<typename ...TComponents>
    class Query {
        private:
            npecs::internal::Storage<Entity>& storage;
        public:
            template<typename ...TExcludeByComponents>
            class ExcludeBy {
                private:
                    internal::Storage<Entity>& storage;
                public:
                    explicit ExcludeBy(internal::Storage<Entity>& storage);
                    typename View<TComponents...>::template ExcludeBy<TExcludeByComponents...> getView();
            };

            explicit Query(internal::Storage<Entity>& storage);

            View<TComponents...> getView();

            template<typename ...TExcludeByComponents>
            ExcludeBy<TExcludeByComponents...> excludeBy();
    };
}
#endif //NPECS_QUERY_HPP
