
#ifndef NPECS_NPECSEXCEPTION_HPP
#define NPECS_NPECSEXCEPTION_HPP

#include <string>
#include <exception>

namespace npecs {

    class NpEcsException : public std::exception {
            std::string details;
        public:
            explicit NpEcsException(std::string details) :
                    details(std::move(details)) {};

#if _MSC_VER
            [[nodiscard]] const char* what() const override {
#else
            [[nodiscard]] inline const char* what() const _GLIBCXX_TXN_SAFE_DYN _GLIBCXX_NOTHROW override {
#endif
                return details.c_str();
            }
    };
}
#endif //NPECS_NPECSEXCEPTION_HPP
