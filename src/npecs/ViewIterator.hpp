
#ifndef NPECS_VIEWITERATOR_HPP
#define NPECS_VIEWITERATOR_HPP

#include <string>

namespace npecs {

    namespace internal {
        template<typename TEntity, typename TEntityFilter, typename ... TComponents>
        class ViewIndex;
    }

    class Entity;

    template<typename TEntityFilter, typename ... TComponents>
    class ViewCursor;

    template<typename TEntityFilter, typename ... TComponents>
    class ViewIterator {
        private:
            internal::ViewIndex<
                    Entity,
                    TEntityFilter,
                    TComponents...
            >* const viewIndex;
        public:
            const std::size_t length;
        public:
            explicit ViewIterator(
                    internal::ViewIndex<
                            Entity,
                            TEntityFilter,
                            TComponents...
                    >* viewIndex,
                    std::size_t length
            );

            ViewCursor<TEntityFilter, TComponents...> begin();

            ViewCursor<TEntityFilter, TComponents...> end();

            ~ViewIterator();
    };
}

#endif //NPECS_VIEWITERATOR_HPP
