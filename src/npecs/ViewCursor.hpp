
#ifndef NPECS_VIEWCURSOR_HPP
#define NPECS_VIEWCURSOR_HPP

#include <tuple>

#include "npecs/internal/types.hpp"

namespace npecs {

    namespace internal {
        template<typename TEntity>
        struct AnyEntity;

        template<typename TEntity, typename TEntityFilter, typename ... TComponents>
        class ViewIndex;
    }

    class Entity;

/**
 * iterator over View, decorated with useful methods
 * @tparam TComponents
 */
    template<typename TEntityFilter, typename ... TComponents>
    class ViewCursor {
        private:
            internal::ViewIndex<Entity, TEntityFilter, TComponents...>* view;
            types::tuple_id idx{};

        public:

            typedef ViewCursor<internal::AnyEntity<Entity>, TComponents...>
                    full;

            template<int i>
            using Type = typename std::tuple_element<i, std::tuple<TComponents...>>::
            type;

            /**
             * returns reference to the existing component associated with matched entity
             *
             * @tparam i type index of the component
             * @return reference to the component
             * @throws exception if entity was already destroyed
             * @throws exception if there is no such component associated with this entity
             */
            template<int i>
            Type<i>& get();

            /**
             *
             * @return matched entity
             */
            const Entity& getEntity();

            bool operator==(const ViewCursor<TEntityFilter, TComponents...>& o) const;

            bool operator!=(const ViewCursor<TEntityFilter, TComponents...>& o) const;

            ViewCursor<TEntityFilter, TComponents...> operator*();

            ViewCursor<TEntityFilter, TComponents...>& operator++();

            ViewCursor<TEntityFilter, TComponents...>& operator--();

            ViewCursor(internal::ViewIndex<typename npecs::Entity, TEntityFilter, TComponents...>* view,
                       types::tuple_id idx);
    };
}

#endif //NPECS_VIEWCURSOR_HPP
