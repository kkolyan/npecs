
#ifndef NPECS_WORLD_HPP
#define NPECS_WORLD_HPP

#include <vector>

#include "npecs/internal/Storage.hpp"
#include "npecs/Entity.hpp"

namespace npecs {

    template<typename ... TComponents>
    class View;

    template<typename ...TComponents>
    class Query;

    template<typename TComponent>
    struct ComponentInfo;

    class World {
        private:
            internal::Storage<Entity> storage{};

        public:
            World() = default;

            World(const World& o) = delete;

            template<typename ... TComponents>
            View<TComponents...> getView();

            template<typename ...TComponents>
            Query<TComponents...> query();

            Entity newEntity();

            /**
             * the purpose is monitoring and testing. it's not optimized
             * to be used as part of main loop.
             * @return handles of all active entitites
             */
            std::vector<Entity> getAllEntities();

            /**
             * the purpose is monitoring and testing. it's not optimized
             * to be used as part of main loop.
             * @return handles of all components of active entitites
             */
            std::vector<typename npecs::ComponentInfo<void>> getAllComponents();

            /**
             * the purpose is monitoring and testing. it's not optimized
             * to be used as part of main loop.
             * @return handles of all components of active entitites
             */
            template<typename TComponent>
            std::vector<typename npecs::ComponentInfo<TComponent>> getComponentsOfType();

            std::size_t deleteUnusedViews();
    };
}
#endif //NPECS_WORLD_HPP
