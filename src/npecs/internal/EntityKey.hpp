
#ifndef NPECS_ENTITYKEY_HPP
#define NPECS_ENTITYKEY_HPP

#include "npecs/internal/types.hpp"

namespace npecs::internal {

    struct EntityKey {

        types::entity_idx idx{};
        types::entity_gen gen{};

        EntityKey() = default;

        EntityKey(
                const types::entity_idx& entityId,
                const types::entity_gen& generation
        );;

        bool operator==(const EntityKey& rhs) const;
        bool operator!=(const EntityKey& rhs) const;

        friend std::ostream& operator<<(std::ostream& os, const EntityKey& entity);
    };

    inline std::ostream& operator<<(std::ostream& os, const EntityKey& entity);
}

#endif //NPECS_ENTITYKEY_HPP
