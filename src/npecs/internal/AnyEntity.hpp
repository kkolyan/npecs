
#ifndef NPECS_ANYENTITY_HPP
#define NPECS_ANYENTITY_HPP

namespace npecs::internal {

    template<typename TEntity>
    struct AnyEntity {
        bool matches(const TEntity& entity);
    };
}
#endif //NPECS_ANYENTITY_HPP
