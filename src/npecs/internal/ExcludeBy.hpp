
#ifndef NPECS_EXCLUDEBY_HPP
#define NPECS_EXCLUDEBY_HPP

namespace npecs::internal {
    template<typename TEntity, typename ...TComponents>
    struct ExcludeBy {
        bool matches(const TEntity& entity);
    };
}

#endif //NPECS_EXCLUDEBY_HPP
