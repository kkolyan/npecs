
#ifndef NPECS_INTERNALCOMPONENTREF_HPP
#define NPECS_INTERNALCOMPONENTREF_HPP

#include "npecs/internal/types.hpp"

namespace npecs::internal {
    template<typename TComponent> struct InternalComponentRef {
        types::entity_idx entityIdx{};
        TComponent* value;
    };
}
#endif //NPECS_INTERNALCOMPONENTREF_HPP
