
#ifndef NPECS_ANYENTITY___HPP
#define NPECS_ANYENTITY___HPP

#include "npecs/internal/AnyEntity.hpp"

namespace npecs::internal {

    template<typename TEntity>
    bool AnyEntity<TEntity>::matches(const TEntity& entity) {
        return true;
    }
}

#endif //NPECS_ANYENTITY___HPP
