
#ifndef NPECS_COMPONENTPOOL___HPP
#define NPECS_COMPONENTPOOL___HPP

#include "npecs/internal/ComponentPool.hpp"
#include "npecs_config.hpp"

namespace npecs::internal {
    template<typename TComponent>
    types::pool_id ComponentPool<TComponent>::getPoolId() {
        static void** ptr = nullptr;
        return types::pool_id(reinterpret_cast<std::size_t>(&ptr));
    }

    template<typename TComponent>
    TComponent& ComponentPool<TComponent>::getComponent(types::entity_idx entityId) {
        const auto& iterator = components.find(entityId);
        if (iterator == components.end()) {
            error("attempt to access non-existing component");
        }
        return iterator->second;
    }

    template<typename TComponent>
    void ComponentPool<TComponent>::destroyComponent(types::entity_idx entityId) {
        components.erase(entityId);
    }

    template<typename TComponent>
    bool ComponentPool<TComponent>::hasComponent(types::entity_idx entityId) {
        return components.count(entityId) > 0;
    }

    template<typename TComponent>
    void ComponentPool<TComponent>::addComponent(types::entity_idx entityId, TComponent& t) {
        const auto& iterator = components.find(entityId);
        if (iterator != components.end()) {
            error("attempt to replace existing component");
        }
        components[entityId] = t;
    }

    template<typename TComponent>
    std::vector<InternalComponentRef<TComponent>>
    ComponentPool<TComponent>::getComponents() {
        std::vector<InternalComponentRef<TComponent>> result;
        for (auto& item : components) {
            result.push_back(InternalComponentRef<TComponent>{item.first, &item.second});
        }
        return result;
    }

    template<typename TComponent>
    std::vector<typename internal::InternalComponentRef<void>>
    ComponentPool<TComponent>::getComponentsRaw() {
        std::vector<InternalComponentRef<void>> result;
        for (auto& item : components) {
            result.push_back(InternalComponentRef<void>{item.first, reinterpret_cast<void*>(&item.second)});
        }
        return result;
    }
}

#endif //NPECS_COMPONENTPOOL___HPP
