
#ifndef NPECS_EXCLUDEBY___HPP
#define NPECS_EXCLUDEBY___HPP

#include "npecs/internal/ExcludeBy.hpp"

namespace npecs::internal {

    template<typename TEntity, typename... TComponents>
    bool ExcludeBy<TEntity, TComponents...>::matches(const TEntity& entity) {
        return (entity.template has<TComponents>() && ...);
    }
}
#endif //NPECS_EXCLUDEBY___HPP
