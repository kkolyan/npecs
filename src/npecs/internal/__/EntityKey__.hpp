
#ifndef NPECS_ENTITYKEY___HPP
#define NPECS_ENTITYKEY___HPP

#include "npecs/internal/EntityKey.hpp"

namespace npecs::internal {
    inline std::ostream& operator<<(std::ostream& os, const internal::EntityKey& entity) {
        os << "EntityKey("
           << "entityId: " << entity.idx
           << ", generation: " << entity.gen
           << ")";
        return os;
    }

    inline internal::EntityKey::EntityKey(
            const types::entity_idx& entityId,
            const types::entity_gen& generation
    ) : idx(entityId),
        gen(generation) {}

    inline bool internal::EntityKey::operator==(const EntityKey& rhs) const {
        return idx == rhs.idx &&
               gen == rhs.gen;
    }

    inline bool internal::EntityKey::operator!=(const EntityKey& rhs) const {
        return !(rhs == *this);
    }
}

#endif //NPECS_ENTITYKEY___HPP
