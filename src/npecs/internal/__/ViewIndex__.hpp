
#ifndef NPECS_VIEWINDEX___HPP
#define NPECS_VIEWINDEX___HPP

#include "npecs/internal/ViewIndex.hpp"

namespace npecs::internal {

    template<typename TEntity, typename TEntityFilter, typename ... TComponents> void
    ViewIndex<TEntity, TEntityFilter, TComponents...>::considerAdd(const TEntity& entity) {
        if (lockCount > 0) {
            delayedAdds.push_back(entity);
            return;
        }
        types::entity_idx entityId = resolveEntityKey(entity).idx;
        if (entities.count(entityId) > 0) {
            return;
        }
        if (!matchesViewCriteria(entity, entityId)) {
            return;
        }
        entity2idx[entityId] = types::tuple_id(idx2Entity.size());
        idx2Entity.push_back(entityId);
        entities[entityId] = entity;
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents> void
    ViewIndex<TEntity, TEntityFilter, TComponents...>::considerRemove(const EntityKey& entity) {
        if (lockCount > 0) {
            delayedRemoves.push_back(entity);
            return;
        }
        auto itr = entity2idx.find(entity.idx);
        if (itr == entity2idx.end()) {
            return;
        }
        if (matchesViewCriteria(entities[entity.idx], entity.idx)) {
            return;
        }
        types::entity_idx lastEntity = idx2Entity.back();
        idx2Entity[NPECS_SIMPLE_TYPE_VALUE(itr->second)] = lastEntity;
        idx2Entity.pop_back();

        entities.erase(entity.idx);
        entity2idx.erase(entity.idx);
    }

    template<typename TEntity, typename TEntityFilter, typename... TComponents> bool
    ViewIndex<TEntity, TEntityFilter, TComponents...>::matchesViewCriteria(const TEntity& entity,
                                                                           types::entity_idx entityIdx) {
        bool matches = true;

        TEntityFilter filter;
        if (!filter.matches(entity)) {
            return false;
        }

        for (int i = 0; i < sizeof...(TComponents); ++i) {
            if (!pools[i]->hasComponent(entityIdx)) {
                matches = false;
                break;
            }
        }
        return matches;
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    void ViewIndex<TEntity, TEntityFilter, TComponents...>::lock() {
        lockCount++;
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    void ViewIndex<TEntity, TEntityFilter, TComponents...>::unlock() {
        lockCount--;
        if (lockCount <= 0) {
            for (const auto& delayedAdd : delayedAdds) {
                considerAdd(delayedAdd);
            }
            for (const auto& delayedRemove : delayedRemoves) {
                considerRemove(delayedRemove);
            }
        }
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    const TEntity& ViewIndex<TEntity, TEntityFilter, TComponents...>::getEntity(types::tuple_id idx) {
        return entities[idx2Entity[NPECS_SIMPLE_TYPE_VALUE(idx)]];
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    types::view_id ViewIndex<TEntity, TEntityFilter, TComponents...>::getTypeId() {
        static void** ptr;
        return types::view_id(reinterpret_cast<std::size_t>(&ptr));
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    ViewIndex<TEntity, TEntityFilter, TComponents...>::ViewIndex(ComponentPool <TComponents>* ... slices) :
            pools{deTypePool(slices)...} {}

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    std::size_t ViewIndex<TEntity, TEntityFilter, TComponents...>::getEntityCount() {
        return idx2Entity.size();
    }

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    template<int i>
    typename ViewIndex<TEntity, TEntityFilter, TComponents...>::template Type<i>&
    ViewIndex<TEntity, TEntityFilter, TComponents...>::getComponent(types::tuple_id tupleId) {
        auto* pool = dynamic_cast<ComponentPool<Type < i>>* > (pools[i]);
        return pool->getComponent(idx2Entity[NPECS_SIMPLE_TYPE_VALUE(tupleId)]);
    }

    template<typename TEntity, typename TEntityFilter, typename... TComponents>
    void ViewIndex<TEntity, TEntityFilter, TComponents...>::acquire() {
        usages++;
    }

    template<typename TEntity, typename TEntityFilter, typename... TComponents>
    void ViewIndex<TEntity, TEntityFilter, TComponents...>::release() {
        usages--;
    }

    template<typename TEntity, typename TEntityFilter, typename... TComponents>
    bool ViewIndex<TEntity, TEntityFilter, TComponents...>::isInUse() {
        return usages > 0;
    }
}

#endif //NPECS_VIEWINDEX___HPP
