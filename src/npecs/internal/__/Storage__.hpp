
#ifndef NPECS_STORAGE___HPP
#define NPECS_STORAGE___HPP

#include <unordered_set>
#include "npecs/internal/Storage.hpp"
#include "npecs/ComponentInfo.hpp"

namespace npecs::internal {

    template<typename TEntity>
    template<typename TComponent>
    void Storage<TEntity>::addComponent(
            const EntityKey& entityKey,
            const TEntity& entity,
            TComponent& t
    ) {

        validateEntityReference(entityKey);

        ComponentPool<TComponent>* pool = getPool<TComponent>();

        if (pool->hasComponent(entityKey.idx)) {
            error("attempt to rewrite component");
        }
        entities[NPECS_SIMPLE_TYPE_VALUE(entityKey.idx)].componentsCount++;
        pool->addComponent(entityKey.idx, t);

        for (auto view: views) {
            view.second->considerAdd(entity);
        }
    }

    template<typename TEntity>
    template<typename TComponent>
    void Storage<TEntity>::removeComponentOf(const EntityKey& entity) {

        validateEntityReference(entity);
        ComponentPool<TComponent>* pool = getPool<TComponent>();
        if (!pool->hasComponent(entity.idx)) {
            error("attempt to delete missing component");
        }
        EntityInfo& entityInfo = entities[NPECS_SIMPLE_TYPE_VALUE(entity.idx)];
        entityInfo.componentsCount--;
        pool->destroyComponent(entity.idx);

        if (entityInfo.componentsCount <= 0) {
            destroyEntity(entity);
        } else {
            for (auto view: views) {
                view.second->considerRemove(entity);
            }
        }
    }

    template<typename TEntity>
    template<typename TComponent>
    TComponent& Storage<TEntity>::getComponentOf(const EntityKey& entity) {
        ComponentPool<TComponent>* pool = getPool<TComponent>();
        validateEntityReference(entity);
        return pool->getComponent(entity.idx);
    }

    template<typename TEntity>
    template<typename TComponent>
    ComponentPool <TComponent>* Storage<TEntity>::getPool() {
        types::pool_id poolId = ComponentPool<TComponent>::getPoolId();
        auto poolItr = pools.find(poolId);
        if (poolItr == pools.end()) {
            pools[poolId] = new ComponentPool<TComponent>();
            poolItr = pools.find(poolId);
        }
        return dynamic_cast<ComponentPool<TComponent>*>(poolItr->second);
    }

    template<typename TEntity>
    template<typename TEntityFilter, typename... TComponents>
    ViewIndex<TEntity, TEntityFilter, TComponents...>* Storage<TEntity>::getViewIndex() {
        types::view_id viewId = ViewIndex<TEntity, TEntityFilter, TComponents...>::getTypeId();
        auto viewItr = views.find(viewId);
        if (viewItr == views.end()) {
            views[viewId] = dynamic_cast<IViewIndex<TEntity>*>(new ViewIndex<TEntity, TEntityFilter, TComponents...>(
                    getPool<TComponents>()...));
            auto entitiesSize = entities.size();
            for (int i = 0; i < entitiesSize; ++i) {
                EntityKey key{types::entity_idx(i), entities[i].generation};
                const TEntity& entity = resolveRichEntityReference(key);
                views[viewId]->considerAdd(entity);
            }
            viewItr = views.find(viewId);
        }
        return dynamic_cast<ViewIndex<TEntity, TEntityFilter, TComponents...>*>(viewItr->second);
    }

    template<typename TEntity>
    template<typename TComponent> std::vector<typename npecs::ComponentInfo<TComponent>>
    Storage<TEntity>::getComponentsOfType() {
        std::vector<ComponentInfo<TComponent>> results;
        for (const auto& item : getPool<TComponent>()->getComponents()) {
            const EntityInfo& entity = entities[NPECS_SIMPLE_TYPE_VALUE(item.entityIdx)];
            results.push_back({EntityKey(item.entityIdx, entity.generation), item.value});
        }
        return results;
    }

    template<typename TEntity>
    template<typename TComponent>
    bool Storage<TEntity>::hasComponentOf(const EntityKey& entity) {
        ComponentPool<TComponent>* pool = getPool<TComponent>();
        return pool->hasComponent(entity.idx);
    }

    template<typename TEntity>
    void Storage<TEntity>::destroyEntity(const EntityKey& entity) {
        validateEntityReference(entity);

        entities[NPECS_SIMPLE_TYPE_VALUE(entity.idx)].generation++;
        freeEntities.push_back(entity.idx);

        for (auto pool: pools) {
            pool.second->destroyComponent(entity.idx);
        }

        for (auto view: views) {
#if _MSC_VER || __clang__
            view.second->considerRemove(entity);
#else
            view.second->template considerRemove(entity);
#endif
        }
    }

    template<typename TEntity>
    void Storage<TEntity>::validateEntityReference(const EntityKey& entity) {
        auto info = entities[NPECS_SIMPLE_TYPE_VALUE(entity.idx)];
        if (info.generation != entity.gen) {
            error("access to destroyed entity");
        }
    }

    template<typename TEntity>
    bool Storage<TEntity>::isAlive(const EntityKey& entity) {
        std::size_t index = NPECS_SIMPLE_TYPE_VALUE(entity.idx);
        return entities[index].generation == entity.gen;
    }

    template<typename TEntity>
    EntityKey Storage<TEntity>::newEntity() {
        EntityKey entity;
        if (!freeEntities.empty()) {

            types::entity_idx& entityId = freeEntities.back();
            freeEntities.pop_back();
            entities[NPECS_SIMPLE_TYPE_VALUE(entityId)].componentsCount = 0;
            entity = EntityKey{entityId, entities[NPECS_SIMPLE_TYPE_VALUE(entityId)].generation};

        } else {
            entity = EntityKey{nextEntityId++, types::entity_gen(1)};
            entities.push_back(EntityInfo{entity.gen, 0});
        }
        return entity;
    }

    template<typename TEntity>
    std::vector<EntityKey> Storage<TEntity>::getAllActiveEntities() {
        std::unordered_set<types::entity_idx> free(
                freeEntities.begin(),
                freeEntities.end()
        );

        std::vector<EntityKey> result;
        for (int i = 0; i < entities.size(); ++i) {
            auto entity = entities[i];
            if (free.count(types::entity_idx(i))) {
                continue;
            }
            result.emplace_back(EntityKey{types::entity_idx(i), entity.generation});
        }
        return result;
    }

    template<typename TEntity>
    std::vector<typename npecs::ComponentInfo<void>> Storage<TEntity>::getAllComponents() {
        std::vector<ComponentInfo<void>> results;
        for (auto& pool : pools) {
            for (const auto& item : pool.second->getComponentsRaw()) {
                auto gen = entities[NPECS_SIMPLE_TYPE_VALUE(item.entityIdx)].generation;
                results.push_back(ComponentInfo<void>{EntityKey(item.entityIdx, gen), item.value});
            }
        }
        return results;
    }

    template<typename TEntity>
    Storage<TEntity>::~Storage() {
        for (const auto& pool : pools) {
            delete pool.second;
        }
        pools.clear();
        for (const auto& view : views) {
            delete view.second;
        }
        views.clear();
    }

    template<typename TEntity>
    std::size_t Storage<TEntity>::deleteUnusedViews() {
        std::vector<types::view_id> toDelete;
        for (auto view : views) {
            if (!view.second->isInUse()) {
                toDelete.push_back(view.first);
            }
        }
        for (auto viewId : toDelete) {
            delete views[viewId];
            views.erase(viewId);
        }
        return toDelete.size();
    }
}

#endif //NPECS_STORAGE___HPP
