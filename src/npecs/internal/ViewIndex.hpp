
#ifndef NPECS_VIEWINDEX_HPP
#define NPECS_VIEWINDEX_HPP

#include <vector>
#include <unordered_map>
#include "npecs/internal/IViewIndex.hpp"
#include "npecs/internal/types.hpp"

namespace npecs::internal {
    struct EntityKey;

    class IComponentPool;

    template<typename TComponent> class ComponentPool;

    template<typename TEntity>
    const EntityKey& resolveEntityKey(const TEntity& entity);

    template<typename TEntity, typename TEntityFilter, typename ... TComponents>
    class ViewIndex : public IViewIndex<TEntity> {

        private:
            IComponentPool* pools[sizeof ...(TComponents)];
            std::unordered_map<types::entity_idx, TEntity> entities;
            std::vector<types::entity_idx> idx2Entity;
            std::unordered_map<types::entity_idx, types::tuple_id> entity2idx;
            std::size_t lockCount{};
            std::vector<TEntity> delayedAdds;
            std::vector<EntityKey> delayedRemoves;
            std::size_t usages{};

        public:
            explicit ViewIndex(ComponentPool<TComponents>* ... slices);

            void considerAdd(const TEntity& entity) override;

            void considerRemove(const EntityKey& entity) override;

            static types::view_id getTypeId();

            const TEntity& getEntity(types::tuple_id idx);

            std::size_t getEntityCount();

            template<int i>
            using Type = typename std::tuple_element<i, std::tuple<TComponents...>>::type;

            template<int i>
            Type<i>& getComponent(types::tuple_id tupleId);

            bool matchesViewCriteria(const TEntity& entity, types::entity_idx entityIdx);

            void lock();

            void unlock();

            void acquire();

            void release();

            bool isInUse() override;

            ~ViewIndex() override = default;

        private:
            template<typename TComponent>
            IComponentPool* deTypePool(ComponentPool<TComponent>* pool) {
                return (IComponentPool*) pool;
            }
    };

    // views without constraint are not supported
    template<typename TEntity, typename TEntityFilter>
    class ViewIndex<TEntity, TEntityFilter> {
    };
}
#endif //NPECS_VIEWINDEX_HPP
