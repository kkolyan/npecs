
#ifndef NPECS_TYPES_HPP
#define NPECS_TYPES_HPP

#include <sstream>
#include <iostream>

// in safe mode compiler protects from mixing different types if IDs.
// it's primarily for development and may be disabled for performance needs.
#define NPECS_TYPES_SAFE

#ifndef NPECS_TYPES_SAFE
#define NPECS_SIMPLE_TYPE(name)\
namespace npecs::types {       \
    typedef std::size_t name;  \
}                                    \

#define NPECS_SIMPLE_TYPE_VALUE(o) { o }
#else

#define NPECS_SIMPLE_TYPE(NAME)                                           \
namespace npecs {                                                                       \
    namespace types {                                                                   \
        class NAME {                                                                    \
            friend class std::hash<NAME>;                                               \
                                                                                        \
            private:                                                                    \
            std::size_t value;                                                                 \
                                                                                        \
                                                                                        \
            public:                                                                     \
                                                                                        \
            std::size_t getValue_DO_NOT_USE_ME() const {                                \
                return value;                                                           \
            }                                                                           \
                                                                                        \
            NAME& operator++() {                                                        \
                ++value;                                                                \
                return *this;                                                           \
            }                                                                           \
                                                                                        \
            const NAME operator++(int) {                                                \
                std::size_t prev = value;                                                      \
                value++;                                                                \
                return NAME(prev);                                                      \
            }                                                                           \
                                                                                        \
            NAME& operator--() {                                                        \
                --value;                                                                \
                return *this;                                                           \
            }                                                                           \
                                                                                        \
            const NAME operator--(int) {                                                \
                std::size_t prev = value;                                                      \
                value--;                                                                \
                return NAME(prev);                                                      \
            }                                                                           \
                                                                                        \
            NAME() : value(std::size_t()) {}                                                   \
                                                                                        \
            explicit NAME(std::size_t value) : value(value) {}                                 \
                                                                                        \
            bool operator==(const NAME& rhs) const {                                    \
                return value == rhs.value;                                              \
            }                                                                           \
                                                                                        \
            bool operator!=(const NAME& rhs) const {                                    \
                return value != rhs.value;                                              \
            }                                                                           \
                                                                                        \
            friend std::ostream& operator<<(std::ostream& os, const NAME& value) {      \
                return os << #NAME << "(" << value.value << ")";                        \
            }                                                                           \
        };                                                                              \
    }                                                                                   \
}                                                                                       \
namespace std {                                                                         \
    template<>                                                                          \
    class hash<npecs::types::NAME> {                                                    \
        public:                                                                         \
        inline std::size_t operator()(const npecs::types::NAME& o) const {              \
            return o.value;                                                             \
        }                                                                               \
    };                                                                                  \
}                                                                                       \

#define NPECS_SIMPLE_TYPE_VALUE(o) o.getValue_DO_NOT_USE_ME()
#endif

NPECS_SIMPLE_TYPE(entity_idx)
NPECS_SIMPLE_TYPE(entity_gen)
NPECS_SIMPLE_TYPE(pool_id)
NPECS_SIMPLE_TYPE(view_id)
NPECS_SIMPLE_TYPE(tuple_id)

#endif //NPECS_TYPES_HPP
