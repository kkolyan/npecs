
#ifndef NPECS_IVIEWINDEX_HPP
#define NPECS_IVIEWINDEX_HPP

namespace npecs::internal {

    struct EntityKey;

    template<typename TEntity>
    class IViewIndex {
        public:
            virtual void considerAdd(const TEntity& entity) = 0;
            virtual void considerRemove(const EntityKey& entity) = 0;
            virtual bool isInUse() = 0;
            virtual ~IViewIndex() = default;
    };
}
#endif //NPECS_IVIEWINDEX_HPP
