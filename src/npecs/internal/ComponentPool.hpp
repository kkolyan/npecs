
#ifndef NPECS_COMPONENTPOOL_HPP
#define NPECS_COMPONENTPOOL_HPP

#include <vector>
#include <unordered_map>

#include "npecs/internal/types.hpp"
#include "npecs/internal/IComponentPool.hpp"

namespace npecs::internal {
    template<typename TComponent> class ComponentPool : public IComponentPool {
        private:
            std::unordered_map<types::entity_idx, TComponent> components;

        public:
            static types::pool_id getPoolId();

            void addComponent(types::entity_idx entityId, TComponent& t);

            TComponent& getComponent(types::entity_idx entityId);

            std::vector<typename npecs::internal::InternalComponentRef<TComponent>> getComponents();

            std::vector<typename npecs::internal::InternalComponentRef<void>> getComponentsRaw() override;

            void destroyComponent(types::entity_idx entityId) override;

            bool hasComponent(types::entity_idx entityId) override;

            ~ComponentPool() override = default;
    };
}
#endif //NPECS_COMPONENTPOOL_HPP
