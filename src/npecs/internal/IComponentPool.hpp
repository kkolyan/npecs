
#ifndef NPECS_ICOMPONENTPOOL_HPP
#define NPECS_ICOMPONENTPOOL_HPP

#include <vector>

#include "npecs/internal/types.hpp"

namespace npecs::internal {
    template<typename TComponent> struct InternalComponentRef;

    class IComponentPool {
        public:
            virtual void destroyComponent(types::entity_idx entityId) = 0;

            virtual bool hasComponent(types::entity_idx entityId) = 0;

            virtual std::vector<typename npecs::internal::InternalComponentRef<void>> getComponentsRaw() = 0;

            virtual ~IComponentPool() = default;
    };
}

#endif //NPECS_ICOMPONENTPOOL_HPP
