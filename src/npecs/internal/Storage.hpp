
#ifndef NPECS_STORAGE_HPP
#define NPECS_STORAGE_HPP

#include <vector>
#include <unordered_map>

#include "npecs/internal/types.hpp"
#include "npecs/internal/EntityKey.hpp"

namespace npecs {
    template<typename TComponent> struct ComponentInfo;
}

namespace npecs::internal {

    template<typename TComponent> class ComponentPool;

    template<typename TEntity>
    class IViewIndex;

    template<typename TEntity, typename TEntityFilter, typename ... TComponents> class ViewIndex;

    template<typename TEntity>
    class Storage {
        public:
            struct EntityInfo {
                types::entity_gen generation{};
                std::size_t componentsCount{};
            };
        private:

            std::unordered_map<types::pool_id, IComponentPool*> pools;

            std::vector<EntityInfo> entities;

            std::vector<types::entity_idx> freeEntities;

            std::unordered_map<types::view_id, typename npecs::internal::IViewIndex<TEntity>*> views;

            types::entity_idx nextEntityId{};

        public:
            Storage() = default;

            Storage(Storage& o) = delete;

            template<typename TComponent>
            void addComponent(const EntityKey& entityKey, const TEntity& entity, TComponent& t);

            template<typename TComponent>
            void removeComponentOf(const EntityKey& entity);

            void destroyEntity(const EntityKey& entity);

            template<typename TComponent>
            TComponent& getComponentOf(const EntityKey& entity);

            template<typename TComponent>
            bool hasComponentOf(const EntityKey& entity);

            bool isAlive(const EntityKey& entity);

            EntityKey newEntity();

            template<typename TEntityFilter, typename... TComponents>
            ViewIndex<TEntity, TEntityFilter, TComponents...>* getViewIndex();

            std::vector<EntityKey> getAllActiveEntities();

            template<typename TComponent>
            std::vector<ComponentInfo<TComponent>> getComponentsOfType();

            std::vector<ComponentInfo<void>> getAllComponents();

            std::size_t deleteUnusedViews();

            ~Storage();

            TEntity resolveRichEntityReference(const EntityKey& entity);

        private:
            template<typename TComponent>
            ComponentPool<TComponent>* getPool();

            void validateEntityReference(const EntityKey& entity);
    };
}
#endif //NPECS_STORAGE_HPP
