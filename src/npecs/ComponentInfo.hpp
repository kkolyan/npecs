
#ifndef NPECS_COMPONENTINFO_HPP
#define NPECS_COMPONENTINFO_HPP

#include "npecs/internal/EntityKey.hpp"

namespace npecs {
    /**
     * used for world introspection only
     */
    template<typename TComponent>
    struct ComponentInfo {
        internal::EntityKey entityKey{};
        TComponent* value;
    };
}
#endif //NPECS_COMPONENTINFO_HPP
