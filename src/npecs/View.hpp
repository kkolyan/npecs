
#ifndef NPECS_VIEW_HPP
#define NPECS_VIEW_HPP

#include "npecs/ViewIterator.hpp"

namespace npecs {
    class Entity;

    namespace internal {

        template<typename TEntity>
        struct AnyEntity;

        template<typename TEntity, typename ...TComponents>
        struct ExcludeBy;

        template<typename TEntity, typename TEntityFilter, typename ... TComponents>
        class ViewIndex;
    }

    class World;

    /**
     * represents a set of logically coupled components associated with the same entity
     *
     * the instance of this class represents a temporary window to a ECS.
     * it provides iterator through entities that has all required components at the moment of
     * the View opening.
     *
     * Note: the set of entities matched by view is recalculated only when all instances of the
     * same View are destroyed, so be careful - all components and entities deleted during View
     * life will be still accessible by iteration of this view, which will certainly lead to exception.
     *
     * @tparam TComponents required components, identified by type
     */
    template<typename ... TComponents>
    class View final {
        private:
            internal::ViewIndex<
                    Entity,
                    internal::AnyEntity<Entity>,
                    TComponents...
            >* const viewIndex;

        public:
            explicit View(
                    internal::ViewIndex<
                            Entity,
                            internal::AnyEntity<Entity>,
                            TComponents...
                    >* viewIndex
            );

            explicit View(World& world);

            ~View();

            ViewIterator<internal::AnyEntity<Entity>, TComponents...> iterate();

            // doesn't make much sense, but introduces redundant complexity, so disabled
            View(const View<internal::AnyEntity<Entity>, TComponents...>& o) = delete;

        public:
            template<typename ...TExcludeByComponents>
            class ExcludeBy {
                private:
                    internal::ViewIndex<
                            Entity,
                            internal::ExcludeBy<Entity, TExcludeByComponents...>,
                            TComponents...
                    >* const viewIndex;

                public:
                    explicit ExcludeBy(
                            internal::ViewIndex<
                                    Entity,
                                    internal::ExcludeBy<Entity, TExcludeByComponents...>,
                                    TComponents...
                            >* viewIndex
                    );

                    explicit ExcludeBy(World& world);

                    ~ExcludeBy();

                    ViewIterator<internal::ExcludeBy<Entity, TExcludeByComponents...>, TComponents...> iterate();

                    // doesn't make much sense, but introduces redundant complexity, so disabled
                    ExcludeBy(const View<internal::ExcludeBy<TExcludeByComponents...>, TComponents...>& o) = delete;
            };
    };

    // views without constraint are not supported
    template<>
    class View<> {
        private:
            View() = default;
    };

}

#endif //NPECS_VIEW_HPP
