
#ifndef NPECS_ENTITY_HPP
#define NPECS_ENTITY_HPP

#include "npecs/internal/EntityKey.hpp"

namespace npecs {

    namespace internal {
        template<typename TEntity>
        class Storage;
    }

    /**
     * the reference to an entity decorated with useful methods.
     */
    class Entity {

        private:
            internal::Storage<Entity>* storage;
        public:
            internal::EntityKey key;

        public:
            Entity();

            Entity(internal::Storage<Entity>* storage, const internal::EntityKey& key);

            /**
             * returns reference to the existing component associated with this entity, identified by specified type.
             *
             * @tparam TComponent type of the component
             * @return reference to the component
             * @throws exception if entity was already destroyed
             * @throws exception if there is no such component associated with this entity
             */
            template<typename TComponent>
            TComponent& get() const;

            /**
             * adds a new component.
             *
             * @tparam TComponent type of component
             * @param t initial component state
             * @return "this" reference for calls chaining
             * @throws exception if entity was already destroyed
             * @throws exception if component of type is already registered for this entity
             */
            template<typename TComponent>
            const Entity& add(TComponent t) const;

            /**
             * removes a component
             *
             * @tparam TComponent type of component to remove
             * @return "this" reference for calls chaining
             * @throws exception if entity was already destroyed
             * @throws exception if there is no component of specified type associated with this entity
             */
            template<typename TComponent>
            void del() const;

            /**
             * safely checks presense of component
             *
             * @tparam TComponent type of component to check
             * @return true if entity is alive and component is present, false otherwise
             */
            template<typename TComponent>
            [[nodiscard]] bool has() const;

            /**
             * safely check entity's health
             *
             * @return false if entity have been already destroyed, true otherwise
             */
            [[nodiscard]] bool isAlive() const;

            /**
             * destroys an entity. all components destroyed too. all references became invalid
             *
             * @throws exception if entity was already destroyed
             */
            void destroy() const;

            bool operator==(const Entity& rhs) const;
            bool operator!=(const Entity& rhs) const;

            friend std::ostream& operator<<(std::ostream& os, const Entity& entity);
    };

    std::ostream& operator<<(std::ostream& os, const Entity& entity);
}
#endif //NPECS_ENTITY_HPP
