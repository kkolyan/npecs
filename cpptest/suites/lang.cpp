
#include "doctest.h"
#include <iostream>
#include <csignal>
#include "boost/stacktrace.hpp"
#include <csetjmp>
#include "unordered_map"

/*
 * this suite is no check significant assumptions about language, compiler, build system.
 */
TEST_SUITE("system demo") {

    TEST_CASE("pointer to reference conversion is ok") {
        class SomeContainer {
            public:
                int value{};
        };
        auto* c = new SomeContainer();
        REQUIRE(c->value == 0);
        SomeContainer& ref = *c;
        ref.value = 42;
        REQUIRE(c->value == 42);
    }

    template<typename ...T> class SomeContainer {
        public:
            int value{};
    };

    TEST_CASE("template pointer to reference conversion is ok") {
        SomeContainer<>* c = (SomeContainer<>*) new SomeContainer<int, float>(); // NOLINT(modernize-use-auto)
        REQUIRE(((SomeContainer<int, float>*) c)->value == 0);
        SomeContainer<int, float>& ref = reinterpret_cast<SomeContainer<int, float>&>(*c); // NOLINT(modernize-use-auto)
        ref.value = 42;
        REQUIRE(((SomeContainer<int, float>*) c)->value == 42);
    }

    TEST_CASE("lifecycle test") {
        static std::string log;
        class SomeClass {
            public:

                explicit SomeClass() {
                    log += "+";
                }

                virtual ~SomeClass() {
                    log += "-";
                }
                static SomeClass create() {
                    return SomeClass();
                }
        };
        {
            const SomeClass& x = SomeClass::create();
            REQUIRE(log == "+");
        }
        REQUIRE(log == "+-");
        log = "";
        {
            SomeClass x = SomeClass::create();
            REQUIRE(log == "+");
        }
        REQUIRE(log == "+-");
    }

    void fall(int height) {
        if (height > 0) {
            fall(height - 1);
        } else {
            int* x = reinterpret_cast<int*>( -5l);
            int y = *x;
        }
    }

    std::jmp_buf buf;
    std::string signalReport;

    TEST_CASE("longjmp works properly") {
        signalReport = "no signals handled";
        std::signal(SIGSEGV, [](int signal) {
            signalReport = "signal handled";
            std::longjmp(buf, 1); // NOLINT(cert-err52-cpp)
        });

        if (!setjmp(buf)) {
            fall(7);
        } else {
            REQUIRE(signalReport == "signal handled");
        }
    }

    template<typename V> struct Entry {
        V* value;
        int key;
    };

    TEST_CASE("unordered_map_element_pointer") {
        std::unordered_map<int, long> map;
        map[0] = 10;
        map[1] = 11;
        map[2] = 12;
        long* value = &map[1];
        REQUIRE(*value == 11);

        *value = 21;
        REQUIRE(map[1] == 21);

        std::vector<Entry<long>> entries;
        entries.reserve(map.size());
        for (auto& item : map) {
            entries.push_back(Entry<long> {&item.second, item.first});
        }
        REQUIRE(entries.size() == 3);

        *entries[1].value = 31;
        REQUIRE(map[1] == 31);
    }
}