
add_library(libbacktrace INTERFACE)

if (UNIX)
    target_compile_definitions(libbacktrace INTERFACE BOOST_STACKTRACE_USE_BACKTRACE)
elseif(LIBBACKTRACE_ENABLED)
    if(MINGW)
        if(NOT MINGW_USR_BIN)
            message(FATAL_ERROR "MINGW_USR_BIN required to
            build libbacktrace. it should point to
            Windows path to /usr/bin of your local GNU env")
        endif()
        message("MinGW detected, so retrieving libbacktrace to improve stacktraces")

        FetchContent_Declare(
                libbacktrace
                GIT_REPOSITORY  https://github.com/ianlancetaylor/libbacktrace.git
                GIT_TAG         master
        )
        FetchContent_GetProperties(libbacktrace)
        if (NOT libbacktrace_POPULATED)
            FetchContent_Populate(libbacktrace)
        endif()

        set(libbacktrace_LIBRARY ${libbacktrace_SOURCE_DIR}/.libs/libbacktrace.a)

        if(NOT EXISTS "${libbacktrace_LIBRARY}")
            FILE(COPY ${PROJECT_SOURCE_DIR}/bash/libbacktrace_make.sh
                    DESTINATION ${libbacktrace_SOURCE_DIR})
            message("building libbacktrace using shell script...")
            execute_process(
                    COMMAND
                        ${MINGW_USR_BIN}/env MSYSTEM=MINGW64
                        ${MINGW_USR_BIN}/bash
                        -l -c 'cd ${libbacktrace_SOURCE_DIR} && ./libbacktrace_make.sh'
                    WORKING_DIRECTORY ${libbacktrace_SOURCE_DIR}
                    )

            if(NOT EXISTS "${libbacktrace_LIBRARY}")
                message(FATAL_ERROR "failed to build libbacktrace")
            endif()
        endif()

        target_link_libraries(libbacktrace INTERFACE ${libbacktrace_LIBRARY})
        target_include_directories(libbacktrace INTERFACE ${libbacktrace_SOURCE_DIR})
        target_compile_definitions(libbacktrace INTERFACE BOOST_STACKTRACE_USE_BACKTRACE)
    elseif()
        message(FATAL_ERROR "libbacktrace linking is not yet implemented for this platform.")
    endif()
elseif()
    message(WARNING "libbacktrace skipped explicitly by LIBBACKTRACE_SKIP option, have fun with binary stacktraces ;)")
endif()
