
macro(np_fetch_library NAME URL ARCHIVE_ENTRIES_FILTER ROOT_ARCHIVE_ENTRY)
    message("Fetching library ${NAME} from ${URL} (${ROOT_ARCHIVE_ENTRY})")

    set(DOWNLOADED_ARCHIVE ${PROJECT_SOURCE_DIR}/_deps/${NAME}.zip)
    set(UNPACKED_ARCHIVE_DIR ${PROJECT_SOURCE_DIR}/_deps/${NAME})

    function(dodo)
        file(LOCK ${PROJECT_SOURCE_DIR}/_deps DIRECTORY
                GUARD FUNCTION)

        if (NOT EXISTS ${DOWNLOADED_ARCHIVE})
            message("downloading ${DOWNLOADED_ARCHIVE}")
            file(DOWNLOAD ${URL} ${DOWNLOADED_ARCHIVE})
        else()
            message("skipping downloading ${DOWNLOADED_ARCHIVE}")
        endif()

        if (NOT EXISTS ${UNPACKED_ARCHIVE_DIR})
            file(MAKE_DIRECTORY ${UNPACKED_ARCHIVE_DIR})
            message("unpacking ${DOWNLOADED_ARCHIVE} to ${UNPACKED_ARCHIVE_DIR}")
            execute_process(COMMAND
                    ${CMAKE_COMMAND} -E tar xvzf "${DOWNLOADED_ARCHIVE}" ${ARCHIVE_ENTRIES_FILTER}
                    WORKING_DIRECTORY ${UNPACKED_ARCHIVE_DIR}
                    )
        else()
            message("skipping unpacking ${DOWNLOADED_ARCHIVE}")
        endif()
    endfunction()

    dodo()

    if (NOT EXISTS ${UNPACKED_ARCHIVE_DIR})
        message(FATAL_ERROR directory not found: ${UNPACKED_ARCHIVE_DIR})
    endif()

    add_library(${NAME} INTERFACE)
    message("target_include_directories ${NAME} INTERFACE ${UNPACKED_ARCHIVE_DIR}${ROOT_ARCHIVE_ENTRY}")
    target_include_directories(${NAME} INTERFACE ${UNPACKED_ARCHIVE_DIR}${ROOT_ARCHIVE_ENTRY})
endmacro()

np_fetch_library(boost
        https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.zip
        boost_1_75_0/boost/*
        /boost_1_75_0
        )

if (UNIX)
    target_link_libraries(boost INTERFACE
            dl
            backtrace
            )
endif()