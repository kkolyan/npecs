
# upgrade is blocked by https://youtrack.jetbrains.com/issue/CPP-23955
set(DOCTEST_VERSION 2.3.8)
FetchContent_Declare(
        doctest
        URL https://github.com/onqtam/doctest/archive/${DOCTEST_VERSION}.zip)

FetchContent_MakeAvailable(doctest)

target_include_directories(doctest INTERFACE $<BUILD_INTERFACE:${doctest_SOURCE_DIR}/doctest>)
