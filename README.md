# ECS framework for C++
## Overview
* No dependencies
* Headers-only
* gcc, MSVC, Clang
* No platform specific features used (tested only on Windows)
* No pointers exposed
* C++17

## Features
* "Sparse sets"-like storage layout (opposed to archetype-based)
* simple entities filtering ("include all of" predicate only)
* safe (generation based) entity compaction
* library provides a framework only for `"EC"` letters from `"ECS"`: there
  is no IEcsSystem interface or something like that. I think, `"S"` from `"ECS"` is 
  the easiest and very individual part - arrange it by yourselves in 
  your favorite way.

## How to use
 all necessary definitions and templates are included via one header file
```c++ 
    #include "npecs.hpp"
```
 components are classes or structs. all user data is intended 
 to be stored as instances of these classes and structs. requirements 
 for these types are the same as for elements of `std::vector`.

```c++ 
    struct Comp1 {
        int x{};
    };

    struct Comp2 {
        int y{};
        float z{};
    };
```

 World is a runtime database of entities and components. 
 You may think of it like it's a table where every column 
 represents a component type and every row represents
 and entity instance, so avery cell is an instance of 
 corresponding component. All user data is intended 
 to be stored in components.

```c++ 
    World world;
```

create entity whenever you want

```c++ 
    Entity e1 = world.newEntity();
```

empty entity doesn't make any sense, so add a components

```c++ 
    e1.add(Comp1{17}).add(Comp2{36});
```

 View is a window to a World.
 Horizontally, view is limited by the set of components identified 
 by template parameters.
 Vertically, view is limited by the set of entities that posess all 
 components required by view.

```c++ 
    View<Comp1> view(world);
    
    for (auto cursor : view.iterate()) {
        // each loop iteration corresponds to the single entity

        // component reference could be retrieved by template parameter index
        int x = cursor.get<0>().x;

        // and written back
        cursor.get<0>().x = 18;
        
        // optional (not specified in View parameters) components 
        // could be accessible via entity
        if (cursor.getEntity().has<Comp2>()) {
            int y = cursor.getEntity().get<Comp2>().y;
        }
    }
```

number of components in view are limited only by the number of
parameters in pack supported by your compiler.

```c++ 
    View<Comp1, Comp2> view(world);
    
    for (auto cursor : view.iterate()) {
        // access to the Comp2 using <1>
        int y = cursor.get<1>().y;

        // component can be deleted (and thus be excluded from this view)
        e1.del<Comp1>();
    } 
```

but we can iterate though Comp2-wielding entities using corresponding view

```c++ 
    View<Comp2> view = world.getView<Comp2>();
    
    for (auto cursor : view.iterate()) {
        int y = cursor.get<0>().y;
        e1.del<Comp2>();
    }
```

Note that entity is destroyed automatically after removal of last component.

You can create Views with entities excluded by some component 
```c++
    world.newEntity().add(Comp3{5});
    world.newEntity().add(Comp3{6}).add(Comp4{});

    View<Comp3>::ExcludeBy<Comp4> view(world);
    
    for (auto cursor: view.iterate()) {
        // Comp3{6} will not be iterated, because has blacklisted Comp4
    }
    // note that exclude constraint is part of View key,
    // so all Comp3 still can be iterated using View without exclusion:
    for (auto cursor: world.getView<Comp3>().iterate()) {
        // all Comp3 will be passed here
    }
```

Also in order to compile you need to define an `npecs::error(const std::string&)` 
function, that is declared by the framework to leave user the choice 
flexibility in error handling. The simplest way:
```c++
    void npecs::error(const std::string& message) {
        throw NpEcsException(message.str());
    }
```
Also check [showcase](test/suites/showcase.cpp) and [tests](test/suites) for details.

## Credits
* Inspired by [LeoECS](https://github.com/Leopotam/ecs) (popular ECS framework for C#).
* [Boost](https://github.com/boostorg/boost) and 
  [libbacktrace](https://github.com/ianlancetaylor/libbacktrace) 
  libraries are used in tests to receive human-readable stacktraces
* [doctest](https://github.com/onqtam/doctest) is used as handy and extremely 
  fast-compiling testing framework.
  