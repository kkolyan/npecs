
#include "doctest.h"
#include "npecs.hpp"
#include "../utils.hpp"

using namespace npecs;


TEST_SUITE("cleanup") {
    TEST_CASE("unused view deleted") {
        struct Comp1 {
        };

        World world;
        {
            View<Comp1> view(world);
        }
        REQUIRE(world.deleteUnusedViews() == 1);
    }

    TEST_CASE("repeated not crashes") {
        struct Comp1 {
        };

        World world;
        {
            View<Comp1> view(world);
        }
        REQUIRE(world.deleteUnusedViews() == 1);
        REQUIRE(world.deleteUnusedViews() == 0);
    }

    TEST_CASE("used view not deleted") {
        struct Comp1 {
        };

        World world;
        View<Comp1> view(world);
        REQUIRE(world.deleteUnusedViews() == 0);
    }

    TEST_CASE("combined") {
        struct Comp1 {
        };
        struct Comp2 {
        };

        World world;
        View<Comp1> view(world);
        {
            View<Comp2> view2(world);
        }
        REQUIRE(world.deleteUnusedViews() == 1);
    }

    TEST_CASE("in time") {
        struct Comp1 {
        };

        World world;
        {
            View<Comp1> view(world);
            REQUIRE(world.deleteUnusedViews() == 0);
        }
        REQUIRE(world.deleteUnusedViews() == 1);
    }

    TEST_CASE("in time with exclude") {
        struct Comp1 {
        };

        struct Comp2 {
        };

        World world;
        {
            View<Comp1>::ExcludeBy<Comp2> view(world);
            REQUIRE(world.deleteUnusedViews() == 0);
        }
        REQUIRE(world.deleteUnusedViews() == 1);
    }
}