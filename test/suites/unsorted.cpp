#include "doctest.h"
#include "npecs.hpp"
#include <sstream>

using namespace npecs;

TEST_SUITE("npecs tests") {

    TEST_CASE("types gives good string representation") {
        std::stringstream s;
        s << types::entity_gen(42);
#ifdef NPECS_TYPES_SAFE
        REQUIRE(s.str() == "entity_gen(42)");
#else
        REQUIRE(s.str() == "42");
#endif
    }

    TEST_CASE("initial entity list is empty") {
        World world;
        REQUIRE(world.getAllEntities().size() == 0);
    }

    TEST_CASE("entity created without crash") {
        World world;
        const Entity& entity = world.newEntity();
        REQUIRE(entity.key.gen == types::entity_gen(1));
        REQUIRE(entity.key.idx == types::entity_idx(0));
    }

    TEST_CASE("empty world iterated") {
        struct Comp1 {
            int x;
        };
        World world;
        int i = 0;
        for (auto row: world.getView<Comp1>().iterate()) {
            i++;
        }
        REQUIRE(i == 0);
    }

    TEST_CASE("empty entity added") {
        World world;
        const Entity& entity = world.newEntity();
        REQUIRE(entity.key.gen == types::entity_gen(1));
        REQUIRE(entity.key.idx == types::entity_idx(0));
    }

    TEST_CASE("entity list increases default view size instantly") {
        World world;
        const Entity& entity = world.newEntity();
        REQUIRE(world.getAllEntities().size() == 1);
    }

    TEST_CASE("empty entity is visible through iteration instantly") {
        World world;
        const Entity& entity = world.newEntity();
        int i = 0;
        for (auto item : world.getAllEntities()) {
            i++;
            REQUIRE(item == entity);
        }
        REQUIRE(i == 1);
    }

    TEST_CASE("different views are different") {
        struct Comp1 {
            int x;
            int y;
        };
        struct Comp2 {
            int x;
            int y;
        };
        REQUIRE(internal::ViewIndex<Entity, internal::AnyEntity<Entity>, Comp2>::getTypeId() !=
                internal::ViewIndex<Entity, internal::AnyEntity<Entity>, Comp1>::getTypeId());
    }

    TEST_CASE("component available by reference via entity from entity") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{1});
        e1.get<Comp1>().x = 12;
        REQUIRE(e1.get<Comp1>().x == 12);
    }

    TEST_CASE("component available by reference via entity from view") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{1});
        e1.get<Comp1>().x = 12;

        auto view = world.getView<Comp1>().iterate();
        auto cursor = view.begin();

        REQUIRE(cursor.get<0>().x == 12);
    }

    TEST_CASE("component available by reference via view from entity") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{1});
        world.getView<Comp1>().iterate().begin().get<0>().x = 12;
        REQUIRE(e1.get<Comp1>().x == 12);
    }

    TEST_CASE("component available by reference via view from view") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{1});
        world.getView<Comp1>().iterate().begin().get<0>().x = 12;

        auto view = world.getView<Comp1>().iterate();
        auto cursor = view.begin();

        REQUIRE(cursor.get<0>().x == 12);
    }
}