#include "doctest.h"
#include "npecs.hpp"
#include "../utils.hpp"

using namespace npecs;


TEST_SUITE("component add") {

    TEST_CASE("component available via getComponentsOfType") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        REQUIRE(world.getComponentsOfType<Comp1>().size() == 1);
        ComponentInfo<Comp1> component = world.getComponentsOfType<Comp1>()[0];
        REQUIRE(component.entityKey == e1.key);
        REQUIRE(component.value->x == 42);
    }

    TEST_CASE("component available via getAllComponents") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        REQUIRE(world.getAllComponents().size() == 1);
        ComponentInfo<void> component = world.getAllComponents()[0];
        REQUIRE(component.entityKey == e1.key);
        REQUIRE(reinterpret_cast<Comp1*>(component.value)->x == 42);
    }

    TEST_CASE("component available via Entity.has") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        REQUIRE(e1.has<Comp1>());
    }

    TEST_CASE("component replacement fails") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        CHECK_THROWS_WITH(e1.add(Comp1{17}), "attempt to rewrite component");
    }

    TEST_CASE("component add failed if entity was explicitly deleted") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.destroy();
        CHECK_THROWS_WITH(e1.add(Comp1{17}), "access to destroyed entity");
    }

    TEST_CASE("component add failed if entity was implicitly deleted") {
        struct Comp1 {
            int x;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        e1.del<Comp1>();
        CHECK_THROWS_WITH(e1.add(Comp1{17}), "access to destroyed entity");
    }

    TEST_CASE("component replacement after delete is ok") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42});
        e1.add(Comp2{8});
        e1.del<Comp1>();
        e1.add(Comp1{17});
        REQUIRE(e1.get<Comp1>().x == 17);
    }

    TEST_CASE("added component is not visible in the current view loop") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity entity = world.newEntity();
        auto view = world.getView<Comp1>().iterate();
        entity.add(Comp1{17});
        REQUIRE(count(view) == 0);
    }

    TEST_CASE("added component is visible in next view loop") {
        struct Comp1 {
            int x;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{17});
        auto view = world.getView<Comp1>().iterate();
        REQUIRE(count(view) == 1);
        REQUIRE(view.begin().get<0>().x == 17);
    }

    TEST_CASE("added component is visible in the current view loop, if not part of criteria") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{17});
        auto view = world.getView<Comp1>().iterate();
        entity.add(Comp2{36});
        auto first = view.begin();
        REQUIRE(first.getEntity().get<Comp2>().y == 36);
    }
}
