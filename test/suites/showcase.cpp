#include "doctest.h"
#include "npecs.hpp"

using namespace npecs;

TEST_SUITE("showcase") {

    TEST_CASE("simple abstract case") {

        // create classes to be a components. they should be compatible with std::vector.
        struct Comp1 {
            int x{};
        };

        // instantiate World, which is a runtime database of entities and components.
        World world;

        // create entity
        Entity e1 = world.newEntity();

        // add a component
        e1.add(Comp1{17});

        int matched = 0;

        /*
         View is a window to a World.
         Horizontally, view is limited by the set of components identified by template parameters.
         Vertically, view is limited by the set of entities that has all components required by view.
         */
        View<Comp1> viewC1(world);
        
        for (auto cursor : viewC1.iterate()) {
            // each loop iteration corresponds to the single entity

            // component reference could be retrieved by template parameter index
            REQUIRE(cursor.get<0>().x == 17);

            // and written back
            cursor.get<0>().x = 18;

            matched++;
        }
        REQUIRE(matched == 1);
        REQUIRE(e1.get<Comp1>().x == 18);

        struct Comp2 {
            int y{};
        };

        // another component (components identified by the type) can be associated with entity
        e1.add(Comp2{36});

        matched = 0;

        // number of components in view are limited only by the number of
        // parameters in pack supported by your compiler.
        View<Comp1, Comp2> viewC1C2 = world.getView<Comp1, Comp2>();
        
        for (auto cursor : viewC1C2.iterate()) {
            REQUIRE(cursor.get<1>().y == 36);

            // component can be deleted
            e1.del<Comp1>();
            matched++;
        }
        REQUIRE(matched == 1);
        REQUIRE(!e1.has<Comp1>());


        matched = 0;
        View<Comp2> view2 = world.getView<Comp2>();
        for (auto cursor : view2.iterate()) {
            REQUIRE(cursor.get<0>().y == 36);
            e1.del<Comp2>();
            matched++;
        }
        REQUIRE(matched == 1);

        // entities are destroyed automatically after removal of last component
        REQUIRE(!e1.isAlive());
    }

    TEST_CASE("explain View lifecycle") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;

        Entity e1 = world.newEntity()
                .add(Comp1{1})
                .add(Comp2{2});

        View<Comp1, Comp2> viewC1C2 = world.getView<Comp1, Comp2>();
        {
            auto view1 = viewC1C2.iterate();

            // delete one of components, so e1 doesn't match view constraint
            for (auto cursor : view1) {
                e1.del<Comp1>();
            }

            /// but...
            int matched = 0;
            for (auto cursor : viewC1C2.iterate()) {
                REQUIRE(!cursor.getEntity().has<Comp1>());
                matched++;
            }
            // it still match!
            REQUIRE(matched == 1);

            // invoking dtor of "view1"
        }
        int matched = 0;
        for (const auto& cursor : viewC1C2.iterate()) {
            matched++;
        }
        // and now view constraint is satisfied
        REQUIRE(matched == 0);
    }
}