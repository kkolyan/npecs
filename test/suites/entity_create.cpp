
#include "doctest.h"
#include "npecs.hpp"

using namespace npecs;

TEST_SUITE("entity create") {
    TEST_CASE("entity created successfully") {
        World world;
        Entity e1 = world.newEntity();
        REQUIRE(world.getAllEntities().size() == 1);
        REQUIRE(world.getAllEntities()[0] == e1);
    }

    TEST_CASE("entity created without components") {
        World world;
        world.newEntity();
        REQUIRE(world.getAllComponents().size() == 0);
    }

    TEST_CASE("new entity created with different ID") {
        World world;
        Entity e1 = world.newEntity();
        Entity e2 = world.newEntity();
        REQUIRE(e1.key.gen == e2.key.gen);
        REQUIRE(e1.key.idx != e2.key.idx);
    }

    TEST_CASE("entity reused") {
        World world;
        Entity e1 = world.newEntity();
        e1.destroy();
        Entity e2 = world.newEntity();
        REQUIRE(e1.key.idx == e2.key.idx);
        REQUIRE(e1.key.gen != e2.key.gen);
    }

    TEST_CASE("entity deleted from the middle") {
        World world;
        Entity entities[10];
        for (auto& entity : entities) {
            entity = world.newEntity();
        }
        entities[7].destroy();
        auto newEntities = world.getAllEntities();
        for (int i = 0; i < 7; ++i) {
            REQUIRE(newEntities[i] == entities[i]);
        }
        for (int i = 7; i < 9; ++i) {
            REQUIRE(newEntities[i] == entities[i + 1]);
        }
    }

    TEST_CASE("entity reused from the middle") {
        World world;
        Entity entities[10];
        for (auto& entity : entities) {
            entity = world.newEntity();
        }
        entities[7].destroy();
        Entity e2 = world.newEntity();
        auto newEntities = world.getAllEntities();
        for (int i = 0; i < 10; ++i) {
            if (i != 7) {
                REQUIRE(newEntities[i] == entities[i]);
            }
        }
        REQUIRE(e2.key == internal::EntityKey{entities[7].key.idx, types::entity_gen(2)});
    }

    TEST_CASE("repeating destroy fails") {
        World world;
        Entity e1 = world.newEntity();
        e1.destroy();
        CHECK_THROWS_WITH(e1.destroy(), "access to destroyed entity");
    }
}