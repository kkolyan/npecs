#include "doctest.h"
#include "npecs.hpp"
#include "../utils.hpp"
#include <vector>

using namespace npecs;


TEST_SUITE("entity destroy") {

    TEST_CASE("entity list updated if entity destroyed") {
        struct Comp1 {
            int x;
        };
        World world;

        world.newEntity().add(Comp1{42});
        world.getView<Comp1>().iterate().begin().getEntity().destroy();
        REQUIRE(world.getAllEntities() == std::vector<Entity>());
    }

    TEST_CASE("entity is evaluated as not alive if destroyed") {
        struct Comp1 {
            int x;
        };
        World world;

        Entity e1 = world.newEntity().add(Comp1{42});
        world.getView<Comp1>().iterate().begin().getEntity().destroy();
        REQUIRE(!e1.isAlive());
    }

    TEST_CASE("view updated after view exit if entity destroyed") {
        struct Comp1 {
            int x;
        };
        World world;

        world.newEntity().add(Comp1{42});
        {
            auto view = world.getView<Comp1>().iterate();
            view.begin().getEntity().destroy();
            REQUIRE(npecs::count(view) == 1);
        }
        auto nextView = world.getView<Comp1>().iterate();
        REQUIRE(npecs::count(nextView) == 0);
    }

    TEST_CASE("components removed after entity destroyed") {
        struct Comp1 {
            int x;
        };
        World world;

        world.newEntity().add(Comp1{42});
        {
            auto view = world.getView<Comp1>().iterate();
            view.begin().getEntity().destroy();
            REQUIRE(world.getAllEntities().size() == 0);
        }
    }
}