#include "doctest.h"
#include "npecs.hpp"
#include "../utils.hpp"

using namespace npecs;


TEST_SUITE("entity filtering") {
    TEST_CASE("filtered entity is excluded from the view") {
        struct Comp1 {
            int x;
        };
        class Comp2 {};
        World world;
        world.newEntity().add(Comp1{5});
        world.newEntity().add(Comp1{6}).add(Comp2{});
        world.newEntity().add(Comp1{7});

        {
            auto view = world.getView<Comp1>().iterate();
            auto cursor = view.begin();

            REQUIRE(cursor.get<0>().x == 5);
            ++cursor;

            REQUIRE(cursor.get<0>().x == 6);
            ++cursor;

            REQUIRE(cursor.get<0>().x == 7);
            ++cursor;

            REQUIRE(cursor == view.end());
        }
        {
            View<Comp1>::ExcludeBy<Comp2> view(world);
            auto iterator = view.iterate();
            auto cursor = iterator.begin();

            REQUIRE(cursor.get<0>().x == 6);
            ++cursor;

            REQUIRE(cursor == iterator.end());
        }

    }
}