#include "doctest.h"
#include "npecs.hpp"
#include "../utils.hpp"

using namespace npecs;


TEST_SUITE("component del") {


    TEST_CASE("component deletion works from entity") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{42}).add(Comp2{17});
        auto view = world.getView<Comp1>().iterate();
        entity.del<Comp1>();
        REQUIRE(world.getComponentsOfType<Comp1>().size() == 0);
    }

    TEST_CASE("component deletion affects Entity.has") {
        struct Comp1 {
            int x;
        };
        struct EntityKeeper {};

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42}).add(EntityKeeper{});
        REQUIRE(e1.has<Comp1>());
        e1.del<Comp1>();
        REQUIRE(!e1.has<Comp1>());
    }

    TEST_CASE("component deletion affects ViewCursor.has") {
        struct Comp1 {
            int x;
        };
        struct EntityKeeper {};

        World world;
        Entity e1 = world.newEntity();
        e1.add(Comp1{42}).add(EntityKeeper{});
        REQUIRE(e1.has<Comp1>());
        e1.del<Comp1>();
        REQUIRE(!e1.has<Comp1>());
    }

    TEST_CASE("component deletion doesn't affect current view") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{42}).add(Comp2{17});
        auto view = world.getView<Comp1>().iterate();
        entity.del<Comp1>();
        REQUIRE(count(view) == 1);
    }

    TEST_CASE("component deletion doesn't affect simultaneous view") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{42}).add(Comp2{17});
        auto view = world.getView<Comp1>().iterate();
        entity.del<Comp1>();
        auto simultView = world.getView<Comp1>().iterate();
        REQUIRE(count(simultView) == 1);
    }

    TEST_CASE("component deletion doesn't affect current view with nested view") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{42}).add(Comp2{17});
        {
            auto view = world.getView<Comp1>().iterate();
            {
                auto nestedView = world.getView<Comp1>().iterate();
                entity.del<Comp1>();
            }
            auto simultView = world.getView<Comp1>().iterate();
            REQUIRE(count(simultView) == 1);

        }
    }
    TEST_CASE("component deletion doesn't affect current view with nested view 2") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        std::cout << "begin" << std::endl;
        entity.add(Comp1{42}).add(Comp2{17});
        {
            auto view = world.getView<Comp1>().iterate();
            {
                auto nestedView = world.getView<Comp1>().iterate();
                entity.del<Comp1>();
                auto simultView = world.getView<Comp1>().iterate();
                REQUIRE(count(simultView) == 1);
            }
            auto simultView = world.getView<Comp1>().iterate();
            REQUIRE(count(simultView) == 1);

        }
        std::cout << "end" << std::endl;
    }

    TEST_CASE("component deletion affects view after exit") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int y;
        };
        World world;
        Entity entity = world.newEntity();
        entity.add(Comp1{42}).add(Comp2{17});
        {
            auto view = world.getView<Comp1>().iterate();
            entity.del<Comp1>();
        }
        auto nextView = world.getView<Comp1>().iterate();
        REQUIRE(count(nextView) == 0);
    }

    TEST_CASE("fail if try access deleted component via view") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int x;
        };
        World world;
        world.newEntity().add(Comp1{42}).add(Comp2{17});

        auto view = world.getView<Comp1>().iterate();
        view.begin().getEntity().del<Comp1>();

        CHECK_THROWS_WITH(view.begin().get<0>(), "attempt to access non-existing component");
    }

    TEST_CASE("fail if try access deleted component via entity") {
        struct Comp1 {
            int x;
        };
        struct Comp2 {
            int x;
        };
        World world;
        world.newEntity().add(Comp1{42}).add(Comp2{17});

        auto view = world.getView<Comp1>().iterate();
        view.begin().getEntity().del<Comp1>();

        CHECK_THROWS_WITH(view.begin().getEntity().get<Comp1>(),
                          "attempt to access non-existing component");
    }

    TEST_CASE("entity destroyed automatically with last component delete") {
        struct Comp1 {
            int x;
        };
        World world;
        world.newEntity().add(Comp1{42});

        auto view = world.getView<Comp1>().iterate();
        view.begin().getEntity().del<Comp1>();
        CHECK_THROWS_WITH(view.begin().getEntity().add(Comp1{}), "access to destroyed entity");
    }

    TEST_CASE("entity list updated instantly if entity destroyed automatically") {
        struct Comp1 {
            int x;
        };
        World world;
        world.newEntity().add(Comp1{42});
        world.getView<Comp1>().iterate().begin().getEntity().del<Comp1>();
        REQUIRE(world.getAllEntities() == std::vector<Entity>());
    }
}