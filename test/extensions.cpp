
#include <sstream>
#include "npecs_config.hpp"
#include "npecs/NpEcsException.hpp"
#include "boost/stacktrace.hpp"

namespace npecs {
    void error(const std::string& shortMessage) {
        std::stringstream s;
        s << shortMessage << "\n" << boost::stacktrace::stacktrace();
        throw NpEcsException(s.str());
    }
}