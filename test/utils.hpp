
#ifndef NPECS_UTILS_HPP
#define NPECS_UTILS_HPP

namespace npecs {

    template<typename T> int count(T& list) {
        int i = 0;
        for (const auto& item: list) {
            i++;
        };
        return i;
    }
}

#endif //NPECS_UTILS_HPP
