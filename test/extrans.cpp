
#include <iostream>
#include "doctest.h"
#include "npecs/NpEcsException.hpp"

REGISTER_EXCEPTION_TRANSLATOR(npecs::NpEcsException & ex) {
    std::cout << "translating exception:\n\n" << ex.what() << std::endl;
    std::string s(ex.what());
    auto firstBreak = s.find('\n');
    if (firstBreak > 0) {
        s = s.substr(0, firstBreak);
    }
    return s.c_str();
}
